﻿#include "Vector.h"

#include <iostream>

std::string Vector::to_string() const
{
    return std::to_string(x_) + ", " + std::to_string(y_) + ", " + std::to_string(z_) + '\n';
}

float Vector::length() const
{
    return static_cast<float>(std::sqrt(std::pow(x_, 2) + std::pow(y_, 2) + std::pow(z_, 2)));
}
