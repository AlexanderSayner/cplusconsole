﻿#pragma once
#include <string>

class Helpers
{
public:
    static int square_sum(int a, int b);
    static std::string gen_random(int len);
    static void even_numbers(const int limit, const bool is_odd);
};
