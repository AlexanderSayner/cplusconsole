﻿#pragma once
#include "Animal.h"

class Fox final :public Animal
{
public:
    std::string voice() override;
};
