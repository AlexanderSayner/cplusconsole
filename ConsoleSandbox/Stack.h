﻿#pragma once

template <typename T>
class stack
{
    T* p_ = nullptr;
    int top_ = -1; // Current top element index
    int capacity_ = 0;

public:
    stack() = default;
    ~stack();

    void init();
    void init(int capacity);

    T pop();
    void push(T element);
    T peek() const;

    int size() const;
    int capacity() const;

private:
    void init_array(int size);
    bool is_full() const;
    bool is_empty() const;
    void expand_array();
    static void empty_stack_failure();
    static void full_stack_failure();
};
