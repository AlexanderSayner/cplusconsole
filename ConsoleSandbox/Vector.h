﻿#pragma once
#include <string>

class Vector
{
public:
    Vector() : x_(0), y_(0), z_(0)
    {
    }

    Vector(const float x, const float y, const float z)
        : x_(x),
          y_(y),
          z_(z)
    {
    }

    std::string to_string() const;
    float length() const;

private:
    float x_;
    float y_;
    float z_;
};
