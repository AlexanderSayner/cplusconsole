﻿#include "Stack.h"

#include <iostream>

// Free memory
template <typename T>
stack<T>::~stack()
{
    delete[]p_;
}

// Default init
template <typename T>
void stack<T>::init()
{
    init_array(5);
}

// Init with initial capacity
template <typename T>
void stack<T>::init(const int capacity)
{
    init_array(capacity);
}

// Alloc memory
template <typename T>
void stack<T>::init_array(const int size)
{
    if (size > 100)
    {
        std::cout << "ERROR: Stack size is too large\n";
        exit(EXIT_FAILURE);
    }
    capacity_ = size;
    p_ = new T[size];
    top_ = -1;
}

// Pop a top element from the stack
template <typename T>
T stack<T>::pop()
{
    // check for stack underflow
    if (is_empty())
    {
        empty_stack_failure();
    }

    std::cout << "DEBUG: Delete element " << peek() << '\n';
    // Decrease stack size by 1
    return p_[top_--];
}

// Add an element to the stack
template <typename T>
void stack<T>::push(const T element)
{
    if (is_full())
    {
        expand_array();
    }

    std::cout << "DEBUG: Insert element " << element << '\n';
    // Increase stack size by 1
    p_[++top_] = element;
}

// The top element of the stack
template <typename T>
T stack<T>::peek() const
{
    if (is_empty())
    {
        empty_stack_failure();
    }

    return p_[top_];
}

// Size of the stack
template <typename T>
int stack<T>::size() const
{
    return top_ + 1;
}

// Capacity of the stack
template <typename T>
int stack<T>::capacity() const
{
    return capacity_;
}

// Check if the stack is full or not
template <typename T>
bool stack<T>::is_full() const
{
    return top_ == capacity_ - 1;
}

// Check if the stack is empty or not
template <typename T>
bool stack<T>::is_empty() const
{
    return top_ == -1;
}

// Reallocate the memory
template <typename T>
void stack<T>::expand_array()
{
    const int new_size = capacity_ + capacity_ / 2;
    if (new_size > 100)
    {
        full_stack_failure();
    }
    std::cout << "> Expanding array from  " << capacity_ << " to " << new_size << '\n';
    const auto temp = new T[new_size];
    std::cout << "> Coping...\n";
    std::copy_n(p_, capacity_, temp);
    std::cout << "> Coping finished, deleting old array...\n";
    delete [] p_;
    std::cout << "> Old array deleted\n";
    p_ = temp;
    capacity_ = new_size;
}

// Empty stack exception handler
template <typename T>
void stack<T>::empty_stack_failure()
{
    std::cout << "ERROR: The stack is empty";
    exit(EXIT_FAILURE);
}

// Full stack exception handler
template <typename T>
void stack<T>::full_stack_failure()
{
    std::cout << "ERROR: The stack is full";
    exit(EXIT_FAILURE);
}
