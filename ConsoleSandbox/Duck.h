﻿#pragma once
#include "Animal.h"

class Duck final : public Animal
{
public:
    std::string voice() override;
};
