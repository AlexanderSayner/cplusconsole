﻿#include "Helpers.h"

#include <algorithm>
#include <complex>
#include <iostream>

int Helpers::square_sum(const int a, const int b)
{
    return static_cast<int>(std::pow(a + b, 2));
}

std::string Helpers::gen_random(const int len)
{
    auto rand_char = []() -> char
    {
        constexpr char charset[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
        constexpr size_t max_index = sizeof charset - 1;
        return charset[rand() % max_index];
    };
    std::string str(len, 0);
    std::generate_n(str.begin(), len, rand_char);
    return str;
}


void Helpers::even_numbers(const int limit, const bool is_odd)
{
    for (int i = is_odd; i <= limit; i += 2)
    {
        std::cout << i << "  ";
    }
}
