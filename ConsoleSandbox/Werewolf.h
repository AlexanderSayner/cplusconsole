﻿#pragma once
#include "Animal.h"

class Werewolf final : public Animal
{
public:
    std::string voice() override;
};
