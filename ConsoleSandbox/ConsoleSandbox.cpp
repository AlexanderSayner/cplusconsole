#include <iostream>

#include "Animal.h"
#include "Duck.h"
#include "Fox.h"
#include "Helpers.h"
#include "Werewolf.h"

using namespace std;

int main(int argc, char* argv[])
{
    Animal* animal[] = {new Duck(), new Werewolf(), new Fox()};
    for (int i = 0; i < 3; i++)
    {
        cout << animal[i]->voice() << '\n';
    }

    return 0;
}
